#!/usr/bin/env escript

% Please watch Tom Scott's video on the Huffman tree algorithm if you
% are not familiar with it:
%
%   https://www.youtube.com/watch?v=JsTptu56GM8

-mode(compile).
-compile(nowarn_unused_function).

-record(leaf,
        {mult  :: pos_integer(),
         value :: any()}).
-record(stem,
        {mult :: pos_integer(),
         l    :: btree(),
         r    :: btree()}).


-type charmults() :: [{char(), pos_integer()}].
-type btree()     :: leaf() | stem().
-type leaf()      :: #leaf{}.
-type stem()      :: #stem{}.


main([]) ->
    main(["www.txt"]);
main([Filename]) ->
    {ok, Binary} = file:read_file(Filename),
    Str = erlang:binary_to_list(Binary),
    %ok = io:format("~s", [Str]),
    Charnums = charmults(Str),
    Charnums_sorted = sort_charmults(Charnums),
    %ok = io:format("~p~n", [Charnums_sorted]),
    Charleafs = [#leaf{value = Char, mult = Mult} || {Char, Mult} <- Charnums_sorted],
    %ok = io:format("~p~n", [Charleafs]),
    Tree = huffman(Charleafs),
    ok = pp_tree(Tree),
    TotalNumSymbols = Tree#stem.mult,
    H = entropy(Charleafs, TotalNumSymbols),
    ACL = avg_codelen_recursive(Tree, 0),
    ACL_Direct = avg_codelen_direct(Tree),
    ok = io:format("entropy                                = ~p~n", [H]),
    ok = io:format("average code length (recursive method) = ~p~n", [ACL]),
    ok = io:format("average code length (direct method)    = ~p~n", [ACL_Direct]),
    ok.


-spec charmults(Str :: string()) -> charmults().
charmults(Str) ->
    charmults(Str, []).


-spec charmults(Str :: string(), Mults :: charmults()) -> charmults().
charmults([], Mults) ->
    Mults;
charmults([C | Rest], Mults) ->
    NewMults =
        case lists:keysearch(C, 1, Mults)  of
            % C has is already been encountered, just increment its multuency
            {value, {_C, CMult}} ->
                NewCMult = CMult + 1,
                NewTuple = {C, NewCMult},
                lists:keystore(C, 1, Mults, NewTuple);
            % first time encountering C, set its multuency equal to 1
            false ->
                NewTuple = {C, 1},
                lists:keystore(C, 1, Mults, NewTuple)
        end,
    charmults(Rest, NewMults).


-spec sort_charmults(charmults()) -> charmults().
sort_charmults(Mults) ->
    lists:keysort(2, Mults).


-spec huffman([btree(), ...]) -> btree().
huffman([Root]) ->
    Root;
huffman([T1, T2 | Rest]) ->
    M1 = mult(T1),
    M2 = mult(T2),
    M  = M1 + M2,
    NewBranch = #stem{l = T1, r = T2, mult = M},
    NewList = insert(NewBranch, Rest),
    huffman(NewList).


-spec mult(btree()) -> pos_integer().
mult(#stem{mult = M}) ->
    M;
mult(#leaf{mult = M}) ->
    M.


-spec insert(btree(), [btree()]) -> [btree()].
insert(Tree, []) ->
    [Tree];
insert(Tree, List) ->
    InitTrees = [Tree | List],
    lists:sort(fun btree_le/2, InitTrees).


-spec btree_le(btree(), btree()) -> boolean().
btree_le(#leaf{mult = ML}, #leaf{mult = MR}) ->
    ML =< MR;
btree_le(#leaf{mult = ML}, #stem{mult = MR}) ->
    ML =< MR;
btree_le(#stem{mult = ML}, #leaf{mult = MR}) ->
    ML =< MR;
btree_le(#stem{mult = ML}, #stem{mult = MR}) ->
    ML =< MR.


-spec pp_tree(btree()) -> ok.
pp_tree(Tree) ->
    pp_tree(Tree, 0).


-spec pp_tree(Tree :: btree(), IndentLevel :: non_neg_integer()) -> ok.
% if it's a newline, print \n instead of the character
pp_tree(#leaf{mult = M, value = $\n }, IndentLevel) ->
    ok = print_indent(IndentLevel),
    ok = io:format("leaf ~p: '\\n'~n", [M]),
    ok;
% printing a leaf is easy, you just print it
pp_tree(#leaf{mult = M, value = C}, IndentLevel) ->
    ok = print_indent(IndentLevel),
    ok = io:format("leaf ~p: '~c'~n", [M, C]),
    ok;
% to print a stem, just print the frequency, then print each branch
pp_tree(#stem{mult = M, l = L, r = R}, IndentLevel) ->
    ok = print_indent(IndentLevel),
    ok = io:format("stem ~p:~n", [M]),
    SubtreesIndentLevel = IndentLevel + 1,
    ok = pp_tree(L, SubtreesIndentLevel),
    ok = pp_tree(R, SubtreesIndentLevel),
    ok.


print_indent(0) ->
    ok;
print_indent(1) ->
    Str = "|   ",
    ok = io:format("~s", [Str]),
    ok;
print_indent(N) ->
    ok = print_indent(1),
    ok = print_indent(N - 1),
    ok.


entropy(Leafs, TotalNumSymbols) ->
    entropy(Leafs, TotalNumSymbols, 0.0).


entropy([], _TotalNumSymbols, Accum) ->
    Accum;
entropy([#leaf{mult = M} | Rest], TotalNumSymbols, Accum) ->
    P = M / TotalNumSymbols,
    EntropyHere = -1 * P * math:log2(P),
    NewAccum = Accum + EntropyHere,
    entropy(Rest, TotalNumSymbols, NewAccum).


% We've traversed down to a leaf, return number of steps
avg_codelen_recursive(#leaf{}, AccCodeLen) ->
    AccCodeLen;
% for a stem, take a weighted average of the code lengths down either
% branch
avg_codelen_recursive(#stem{l = L, r = R}, AccCodeLen) ->
    MultL = mult(L),
    MultR = mult(R),
    Sum   = MultL + MultR,
    PrL   = MultL / Sum,
    PrR   = MultR / Sum,
    ACL_L = avg_codelen_recursive(L, AccCodeLen + 1),
    ACL_R = avg_codelen_recursive(R, AccCodeLen + 1),
    (PrL * ACL_L) + (PrR * ACL_R).


avg_codelen_direct(Tree = #stem{mult = TotalNumSymbols}) ->
    avg_codelen_direct(Tree, TotalNumSymbols, 0).


% this just traverses down until it gets a leaf, and computes
% probability(leaf) * codelen, adds it up for every leaf
-spec avg_codelen_direct(btree(), TotalNumSymbols :: integer(), Depth :: integer()) -> float().
avg_codelen_direct(#leaf{mult = M}, TotalNumSymbols, Depth) ->
    Pr = M / TotalNumSymbols,
    ACL_this = Pr * Depth,
    ACL_this;
% for a stem, increment the depth, and take the sum over the left and
% right branches
avg_codelen_direct(#stem{l = L, r = R}, TotalNumSymbols, Depth) ->
    ACL_L = avg_codelen_direct(L, TotalNumSymbols, Depth + 1),
    ACL_R = avg_codelen_direct(R, TotalNumSymbols, Depth + 1),
    ACL_L + ACL_R.
